Agenda for Saturday
====================================================
08:30 AM - Breakfast <br>
09:00 AM - Welcome: Introductions, Why Clojure & Using git to save your program <br>
10:00 AM - Track 1 & Track 2 - Workshop Part I <br>
10:30 AM - Break <br>
11:15 AM - Panel Talk: Ask any questions you have for software development professionals <br>
12:00 PM - Group Photo & Lunch <br>
01:00 PM - Workshop Part II <br>
02:30 PM - Break <br>
03:00 PM - Workshop Part III <br>
04:00 PM - Wrap Up and [Survey](https://docs.google.com/forms/d/e/1FAIpQLSdWZxH9QkFnxeRk0NV67zeKPCT5usrYr-Qx15dh3nh_qyw8aQ/viewform?usp=sf_link)<br>
04:30 PM - Celebration: Informal Gathering at [Crooked Pint](https://www.crookedpint.com/downtown-minneapolis/)
