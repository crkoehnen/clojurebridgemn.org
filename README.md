# clojurebridgemn.org

The static site for clojurebridgemn.

    https://clojurebridgemn.org

## Running it

### Requirements

You need boot to install this. See the Install section of:

    https://github.com/boot-clj/boot

You can build and serve with the following (changes will be auto-reloaded):

    boot dev

You can now visit `localhost:3000` to see the website.

Want to generate static HTML files in the `target` directory?

    boot build-prod

Run the code formatter:

    boot fmt --git --mode overwrite --really

Check for new dependencies:

    boot -d boot-deps ancient
