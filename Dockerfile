FROM clojure:latest


RUN bash -c "cd /usr/local/bin && curl -fsSLo boot https://github.com/boot-clj/boot-bin/releases/download/latest/boot.sh && chmod 755 boot"

COPY . /code 
RUN cd /code && BOOT_AS_ROOT=yes boot build-prod
